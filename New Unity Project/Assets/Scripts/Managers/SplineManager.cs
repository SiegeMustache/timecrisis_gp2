﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineManager : Singleton<SplineManager> {

    public GameObject[] splines;
    public SplineWalker playerWalker;
    [HideInInspector]
    public SplineData currentSpline;
    SplineData headSpline;
    [HideInInspector]
    public bool gameEnded;

	// Use this for initialization
	void Start () {
		for(int i = 0; i < splines.Length; i++)
        {
            AddSpline(splines[i]);
        }
        currentSpline = headSpline;
        playerWalker.spline = currentSpline.spline.GetComponent<BezierSpline>();
        gameEnded = false;
	}
	
	// Update is called once per frame
	void Update () {

        if (playerWalker.progress >= 1 && currentSpline.spline.GetComponent<BezierSpline>().Loop == false && currentSpline.edge.nextSpline != null && SplineWalker.instance.inCombat == false)
        {
            NextSpline();
            SplineWalker.instance.spline = currentSpline.spline.GetComponent<BezierSpline>();
            playerWalker.progress = 0;
            playerWalker.spline = currentSpline.spline.GetComponent<BezierSpline>();
        }
        else if (playerWalker.progress >= 1 && currentSpline.spline.GetComponent<BezierSpline>().Loop == false && SplineWalker.instance.inCombat == true)
        {
            playerWalker.progress = 1;
        }
        else if(1 - playerWalker.progress < 0.1 && SplineWalker.instance.inCombat == false)
        {
            HideController.instance.RegisterSpots();
            HideController.instance.StoreResetPosition();
        }
        else if(currentSpline.edge.nextSpline == null && playerWalker.progress >= 1 && SplineWalker.instance.inCombat == true)
        {
            playerWalker.progress = 1;
        }
	}

    private void AddSpline(GameObject spline)
    {
        if (headSpline == null)
        {
            headSpline = new SplineData(spline);
            return;
        }
        SplineData currentSplineData = headSpline;
        while(currentSplineData.edge.nextSpline != null)
        {
            currentSplineData = currentSplineData.edge.nextSpline;
        }
        if(currentSplineData.edge.nextSpline == null)
        {
            currentSplineData.edge.nextSpline = new SplineData(spline);
            return;
        }
    }

    public void NextSpline()
    {
        currentSpline = currentSpline.edge.nextSpline;
    }

}
