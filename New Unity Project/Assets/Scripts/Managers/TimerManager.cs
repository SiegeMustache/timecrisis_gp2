﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerManager : Singleton<TimerManager> {

    public float time;
    [HideInInspector]
    public float timer;
    [HideInInspector]
    public int minutes;
    [HideInInspector]
    public int seconds;
	// Use this for initialization
	void Start () {
        timer = time;
	}
	
	// Update is called once per frame
	void Update () {

        if(timer > 0 && PlayerManager.instance.currentHealth > 0 && SplineManager.instance.gameEnded == false)
        {
            timer -= Time.deltaTime;
        }

        minutes = Mathf.FloorToInt(timer / 60);
        seconds = Mathf.FloorToInt(timer - (minutes * 60));
	}
}
