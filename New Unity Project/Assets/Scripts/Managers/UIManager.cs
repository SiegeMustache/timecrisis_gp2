﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager> {

    public Text ammoText;
    public Text healthText;
    public Text timerText;
    public GameObject defeatScreen;
    public GameObject winScreen;

	// Use this for initialization
	void Start () {
        defeatScreen.SetActive(false);
        winScreen.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

        ammoText.text = "AMMO : " + ShootManager.instance.currentAmmo.ToString() + " / " + ShootManager.instance.maxAmmo.ToString();
        healthText.text = "HP : " + PlayerManager.instance.currentHealth.ToString() + " / " + PlayerManager.instance.maxHealth.ToString();
        timerText.text = TimerManager.instance.minutes.ToString() + " : " + TimerManager.instance.seconds.ToString();

        if(PlayerManager.instance.currentHealth <= 0 || TimerManager.instance.timer <= 0)
        {
            defeatScreen.SetActive(true);
        }
        if(SplineManager.instance.gameEnded == true)
        {
            winScreen.SetActive(true);
        }
	}
}
