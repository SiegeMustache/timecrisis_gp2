﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject enemy;
    GameObject currentEnemy;
    public int quantity;
    public GameObject destination;
    public GameObject spawnLocation;
    public BezierSpline spline;

	// Use this for initialization
	void Start () {
        destination.GetComponent<MeshRenderer>().enabled = false;
        spawnLocation.GetComponent<MeshRenderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if(SplineWalker.instance.spline == spline && SplineWalker.instance.progress >= 1f)
        {

            if (currentEnemy == null && quantity > 0)
            {
                quantity--;
                currentEnemy = Instantiate(enemy, spawnLocation.transform.position, Quaternion.identity);
                currentEnemy.GetComponent<EnemyBehavior>().destination = destination;
            }
            if (quantity >= 0 && currentEnemy != null)
            {
                if (!SplineWalker.instance.currentSpawners.Contains(this.gameObject))
                {
                    SplineWalker.instance.currentSpawners.Add(this.gameObject);
                }
            }
            if (currentEnemy != null)
            {
                SplineWalker.instance.inCombat = true;
            }
            if(currentEnemy == null && quantity <= 0)
            {
                SplineWalker.instance.currentSpawners.Remove(this.gameObject);
                if (SplineWalker.instance.currentSpawners.Count == 0 && SplineManager.instance.currentSpline.edge.nextSpline == null)
                {
                    SplineManager.instance.gameEnded = true;
                }

            }
        }
	}
}
