﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootManager : Singleton<ShootManager> {

    public int maxAmmo;
    [HideInInspector]
    public int currentAmmo;
    public int damage;
    public float attackSpeed;
    float attackSpeed_Timer;
    public float reloadSpeed;
    float reloadSpeed_Timer;
    bool isReloading = false;
    public Camera myCamera;
    public LayerMask enemyLayer;
	// Use this for initialization
	void Start () {
		if(maxAmmo <= 0)
        {
            maxAmmo = 1;
        }
        currentAmmo = maxAmmo;
        attackSpeed_Timer = 0;
        reloadSpeed_Timer = 0;
        if(attackSpeed <= 0)
        {
            attackSpeed = 1;
        }
        if(reloadSpeed <= 0)
        {
            reloadSpeed = 1;
        }
        isReloading = false;
	}
	
	// Update is called once per frame
	void Update () {
        if(PlayerManager.instance.currentHealth > 0)
        {
            attackSpeed_Timer += Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.R) && currentAmmo < maxAmmo && isReloading == false && HideController.instance.isHiding == false)
            {
                isReloading = true;
            }
            if (isReloading == true && HideController.instance.isHiding == false)
            {
                reloadSpeed_Timer += Time.deltaTime;
                if (reloadSpeed_Timer >= reloadSpeed)
                {
                    Reload();
                    isReloading = false;
                    reloadSpeed_Timer = 0;
                }
            }
            if (Input.GetKeyDown(KeyCode.Mouse0) && currentAmmo > 0 && isReloading == false && HideController.instance.isHiding == false)
            {
                Shoot();
            }
        }
	}

    private void Reload()
    {
        currentAmmo = maxAmmo;
    }

    private void Shoot()
    {
        currentAmmo--;
        RaycastHit hit;
        Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, enemyLayer))
        {
            if(hit.collider.gameObject.tag == "Enemy")
            {
                hit.collider.gameObject.GetComponent<EnemyBehavior>().TakeDamage(damage);
            }
        }
    }
}
