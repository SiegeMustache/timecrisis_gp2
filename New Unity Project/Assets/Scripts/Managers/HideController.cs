﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideController : Singleton<HideController> {

    [HideInInspector]
    public Vector3 startMarker;
    [HideInInspector]
    public Vector3 endMarker;
    [HideInInspector]
    public Vector3 resetPosition;
    public float hideOffset;
    [HideInInspector]
    public bool isHiding;
    // Movement speed in units/sec.
    public float speed = 1.0F;

    private void Start()
    {
        isHiding = false;
    }

    // Follows the target position like with a spring
    void Update()
    {
        if(SplineWalker.instance.inCombat == true && PlayerManager.instance.currentHealth > 0)
        {
            if(Input.GetKey(KeyCode.Space))
            {
                isHiding = true;
                if(Vector3.Distance(transform.position, endMarker) > 0.1)
                {
                    // The step size is equal to speed times frame time.
                    float step = speed * Time.deltaTime;

                    // Move our position a step closer to the target.
                    transform.position = Vector3.MoveTowards(transform.position, endMarker, step);
                }
            }
            else if(!Input.GetKey(KeyCode.Space))
            {
                isHiding = false;
                // Move our position a step closer to the target.
                transform.position = resetPosition;   
            }
        }
    }

    public void StoreResetPosition()
    {
        resetPosition = new Vector3(SplineWalker.instance.transform.position.x, SplineWalker.instance.transform.position.y, SplineWalker.instance.transform.position.z);
    }

    public void RegisterSpots()
    {
        startMarker = new Vector3(SplineWalker.instance.transform.position.x, SplineWalker.instance.transform.position.y, SplineWalker.instance.transform.position.z);
        endMarker = new Vector3(SplineWalker.instance.transform.position.x, SplineWalker.instance.transform.position.y - hideOffset, SplineWalker.instance.transform.position.z);
    }
}
