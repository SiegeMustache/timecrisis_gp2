﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineEdge{

    public SplineData nextSpline;

    public SplineEdge()
    {
        nextSpline = null;
    }

}
