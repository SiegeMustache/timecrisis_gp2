﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineData {

    public GameObject spline;
    public SplineEdge edge;

    public SplineData()
    {
        spline = null;
        edge = new SplineEdge();
    }

    public SplineData(GameObject newSpline)
    {
        spline = newSpline;
        edge = new SplineEdge();
    }
}
