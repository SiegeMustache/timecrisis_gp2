﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : Singleton<PlayerManager> {

    public int maxHealth;
    [SerializeField]
    public int currentHealth;
    public GameObject playerBody;
    public GameObject[] playerMissBoxes;

	// Use this for initialization
	void Start () {
        currentHealth = maxHealth;
	}
	
	// Update is called once per frame
	void Update () {
        if (currentHealth <= 0)
        {
            Debug.Log("DEAD");
        }
    }

    public void TakeDamage()
    {
        Debug.Log("HIT");
        currentHealth--;
    }
}
