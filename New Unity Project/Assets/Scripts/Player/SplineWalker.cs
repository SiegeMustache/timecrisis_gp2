﻿using System.Collections.Generic;
using UnityEngine;

public class SplineWalker : Singleton<SplineWalker> {

	public BezierSpline spline;

	public float duration;

	public bool lookForward;
    public GameObject target;

	public SplineWalkerMode mode;

    [HideInInspector]
	public float progress;
	private bool goingForward = true;
    [HideInInspector]
    public List<GameObject> currentSpawners = new List<GameObject>();
    [HideInInspector]
    public bool inCombat = false;

    private void Start()
    {
        if(target == null && lookForward == false)
        {
            lookForward = true;
        }
        else if(target != null && lookForward == true)
        {
            lookForward = false;
        }
    }

    private void Update () {

	    if (goingForward) {
            if(PlayerManager.instance.currentHealth > 0 && TimerManager.instance.timer > 0)
            {
                progress += Time.deltaTime / duration;
            }
		    if (progress > 1f) {
			    if (mode == SplineWalkerMode.Once) {
				    progress = 1f;
			    }
			    else if (mode == SplineWalkerMode.Loop) {
				    progress -= 1f;
			    }
			    else {
				    progress = 2f - progress;
				    goingForward = false;
			    }
		    }
	    }
	    else {
		    progress -= Time.deltaTime / duration;
		    if (progress < 0f) {
			    progress = -progress;
			    goingForward = true;
		    }
	    }

        Vector3 position = spline.GetPoint(progress);
        transform.localPosition = position;
        if (lookForward == true || target == null)
        {
            transform.LookAt(position + spline.GetDirection(progress));
        }
        else if (lookForward == false && target != null)
        {
            transform.LookAt(target.transform);
        }
        


        if (currentSpawners.Count > 0)
        {
            inCombat = true;
        }
        else if(currentSpawners.Count <= 0)
        {
            inCombat = false;
        }

        Debug.Log(currentSpawners.Count.ToString());
        if(currentSpawners.Count <= 0)
        {
            Debug.Log("No Enemies");
        }
	}
}