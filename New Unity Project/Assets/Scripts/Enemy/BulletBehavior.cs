﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour {

    public GameObject target;
    Vector3 destination = new Vector3();
    [HideInInspector]
    public float speed;
    float timer;
	// Use this for initialization
	void Start () {
        destination = new Vector3(target.transform.position.x, target.transform.position.y, target.transform.position.z);
        timer = 0;
	}
	
	// Update is called once per frame
	void Update () {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, destination, step);

        timer += Time.deltaTime;
        if(timer >= 10)
        {
            Death();
        }
        if(transform.position == destination)
        {
            Death();
        }
    }

    private void Death()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            PlayerManager.instance.TakeDamage();
            Death();
        }
        else
        {
            Death();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.gameObject.tag != "Player")
        {
            Death();
        }
    }
}
