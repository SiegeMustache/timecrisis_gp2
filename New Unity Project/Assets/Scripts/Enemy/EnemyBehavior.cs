﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyBehavior : MonoBehaviour {

    [HideInInspector]
    public int currentHealth;
    public int maxHealth;
    public int chanceOfMiss;
    public float attackSpeed;
    public int timeBonus;
    float attackSpeed_Timer;
    public GameObject bullet;
    public GameObject bulletSpawnPoint;
    public float bulletSpeed;
    [HideInInspector]
    public GameObject destination;
    NavMeshAgent agent;

	// Use this for initialization
	void Start () {
        currentHealth = maxHealth;
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
	
        if(currentHealth <= 0)
        {
            Death();
        }
        if(Vector3.Distance(transform.position, destination.transform.position) <= 1f)
        {
            attackSpeed_Timer += Time.deltaTime;
            transform.LookAt(PlayerManager.instance.playerBody.transform.position);
            if(attackSpeed_Timer >= attackSpeed && PlayerManager.instance.currentHealth > 0)
            {
                int chanceCheck = Random.Range(0, 101);
                if (chanceCheck <= chanceOfMiss)
                {
                    Shoot(PlayerManager.instance.playerMissBoxes[Random.Range(0, PlayerManager.instance.playerMissBoxes.Length)], bulletSpeed * 2);
                }
                else if(chanceCheck > chanceOfMiss)
                {
                    Shoot(PlayerManager.instance.playerBody, bulletSpeed);
                }
            }
        }
        else if(Vector3.Distance(transform.position, destination.transform.position) > 1f)
        {
            agent.SetDestination(destination.transform.position);
        }
	}

    private void Death()
    {
        TimerManager.instance.timer += timeBonus;
        Destroy(this.gameObject);
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
    }

    private void Shoot(GameObject target, float bulletSpeed)
    {
        Instantiate(bullet, bulletSpawnPoint.transform.position, Quaternion.identity);
        bullet.GetComponent<BulletBehavior>().target = target;
        bullet.GetComponent<BulletBehavior>().speed = bulletSpeed;
        attackSpeed_Timer = 0;
    }
}
